<?php

namespace Database\Factories;

use App\Models\Patient;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Factories\Factory;

class PatientFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Patient::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $bloodTypes = ['A', 'B', 'O'];

        return [
            'blood_type' => Arr::random($bloodTypes),
        ];
    }
}
